#include <stdio.h>
// series26
// Даны целые числа K, N и набор из N вещественных чисел: A1...AN. Вывести К-е степени чисел данного набора

int main(void)
{
    int k, n, i, j;
    float a;

    printf("Enter N\n");
    scanf ("%d", &n);
    printf ("Enter K\n");
    scanf ("%d", &k);

    // Цикл: считывание элемента - обработка - вывод результата
    for (i=0; i<n; i++) {
        scanf ("%f", &a);

        // Возведение в степень
        float prod=1;
        for (j=1; j<=k; j++)
            prod *= a;

        printf ("%f\n", prod);
    }
    return 0;
}

