#include <stdio.h>
#include <string.h>

/* В файле содержатся данные о товарах в интернет-магазине (название, артикул, цена, валюта (рубли, доллары, тенге), номер склада).

Для товаров, хранящихся на одном складе (номер задается пользователем) создать файл, в котором:
 - цена для всех товаров будет переведена в рубли
 - товары будут отсортированы по цене */

struct Data{
    char name[50];
    char art[50];
    float price;
    char val[4];
    int stock;
};

float convertPrice(float price, char* val);

int main(void)
{
    int stock=11111, size;
    struct Data List [100];
     FILE* fd = fopen ("abc.txt", "r");
     int i=0;
     while (!feof(fd)) {
         fscanf (fd, "%s", List[i].name);
         fscanf (fd, "%s", List[i].art);
         fscanf (fd, "%f", &(List[i].price)); // амперсанды нужны, а мы про них забыли. List[i].price - это же просто переменная типа float
         fscanf (fd, "%s", List[i].val);
         fscanf (fd, "%d", &(List[i].stock));
         i++;
     }
     size = i - 1;

     printf("Was\n");
     for (i = 0; i < size; i++){ // Все-таки надо указывать строго меньше size - потому что последний элемент с конца файла считывается "нулевым" (можешь проверить)
         printf("%s, %s, %f, %s, %d\n", List[i].name, List[i].art, List[i].price, List[i].val, List[i].stock);
     }

     for (i=0; i < size; i++){
         if (List[i].stock == stock) {
             List[i].price = convertPrice(List[i].price, List[i].val);
             strcpy(List[i].val, "rub");
         }
     }

     // TO DO: сортировка массива

     // TO DO: запись в файл

     printf("Now\n");
     for (i = 0; i < size; i++){
         printf("%s, %s, %f, %s, %d\n", List[i].name, List[i].art, List[i].price, List[i].val, List[i].stock);
     }
    return 0;
}

float convertPrice(float price, char *val){
    if (!strcmp(val, "usd")) { // Если strcmp вернет 0, то строки равны (поэтому ставим "!")
      return price*58;
    }
    if (!strcmp(val, "tg")) {
        return price*0.17;
    }
    return price;
}

