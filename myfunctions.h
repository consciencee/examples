#ifndef MYFUNCTIONS
#define MYFUNCTIONS

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N 3
#define M 5

void output(double mat1 [N][M]);
double getrandom();

#endif // MYFUNCTIONS

