#include <stdio.h>
#include <string.h>

struct CarInfo{
    int price;
    char name[100];
};

/* чтобы каждый раз не писать struct:
 *
 * typedef struct CarInfo{
    int price;
    char name[100];
}CarInfo;*/

int getAllPrice(struct CarInfo *arr, int size);
void bubblesort(struct CarInfo *arr, int n);
void writeToFile(struct CarInfo* arr, int size);

int main(void)
{
    const char *fileName = "file";
    FILE* fd = fopen(fileName, "r"); // исходный файл с данными открываем в режиме чтения

    if(fd == NULL){ // проверили на то, что открылся корректно
        printf("Error\n");
        return 1;
    }

    struct CarInfo data[500];
    int i = 0, size = 0;

    // цикл считывания - до конца файла, заранее количество записей в файле неизвестно
    // (при посимвольном считывании с помощью getc можно также сравнивать очередной символ с EOF)
    while(!feof(fd)){
        // данные считываются в очередной элемент массива
        fscanf(fd, "%s", data[i].name);
        fscanf(fd, "%d", &(data[i].price));
        i++;
    }

    size = i - 1;

    printf("price %d\n", getAllPrice(data, size)); // data - по определению массива - является указателем на первый элемент массива

    bubblesort(data, size);

    writeToFile(data, size);

    fclose(fd);

    return 0;
}

int getAllPrice(struct CarInfo *arr, int size)
{
    int result = 0, i;

    for(i = 0; i < size; i++, arr++)
        result += arr->price; // стрелка используется для доступа к полям структуры с помощью указателя. То есть как точка, только стрелка

    return result;
}

void bubblesort(struct CarInfo *a, int n)
{
  int j, nn;

  do {
    nn = 0;
    for (j = 1; j < n; ++j)
      if (a[j-1].price > a[j].price) {
          struct CarInfo buf = a[j];
          a[j] = a[j-1];
          a[j-1] = buf;
          nn = j;
      }
    n = nn;
  } while (n);
}

void writeToFile(struct CarInfo* arr, int size)
{
    FILE* fd = fopen("fileOut", "w");
    int i = 0;

    // вывод в файл значений всех элементов, на которые указывает arr
    while(i < size){
        fprintf(fd, "%s ", arr->name);
        fprintf(fd, "%d ", arr->price);
        i++;
        arr++;
    }

    fclose(fd);
}
