#include <stdio.h>
#define SIZE 5

int main(void)
{
    int s = 6;
    int arr[s];
    int max, i;

    printf("Input %d elements, please\n", SIZE);

    for(i=0; i<=SIZE - 1; i++){
        scanf("%d", &arr[i]);
    }
    max=arr[0];
    for(i=1; i<=SIZE - 1; i++){
        if (arr[i]>max)
            max=arr[i];
    }
    printf("%d", max);
    return 0;
}

