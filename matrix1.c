#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#define N 7
#define M 4

int main(void)
{
    int mat[N][M];
    int min;

    srand(time(NULL));//запустили рандом
    for(int i=0; i<N; i++){// initialize
        for(int j=0; j<M; j++)
            mat[i][j] = rand()%10;
    }
    min = mat[0][0];
    for(int i=0; i<N; i++){//поиск минимума
        for(int j=0; j<M; j++){
            if (mat[i][j]<min)
                min = mat[i][j];
        }

    }
    for(int i=0; i<N; i++){//вывод матрицы
        for(int j=0; j<M; j++){
    printf("%d ", mat[i][j]);
        }
        printf("\n");
    }
    printf("%d", min);
    return 0;
}

