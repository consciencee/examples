#include <stdio.h>

#define MAX_SIZE 512

int main(void)
{
    // Ищем длину строки

    char str[MAX_SIZE];

    int cnt = 0, i = 0;
    printf("enter string\n");
    scanf("%s", str); // Тут вводится строка. После последнего символа в строке будет '\0'

    while (*(str + i) != '\0'){
        cnt++;
        i++;
    }
    printf ("%d\n", cnt);

    return 0;
}

