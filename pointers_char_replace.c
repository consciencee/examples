#include <stdio.h>

#define MAX_SIZE 512

int main(void)
{
    // Поменять местами i-е символы строк

    char str1[MAX_SIZE], str2[MAX_SIZE];// У этих строк - константные указатели, указывающие на их начало
    char *ptr1 = str1, *ptr2 = str2, buf; //Создаем новые указатели и инициализируем их

    int i;

    scanf("%s", str1);
    scanf("%s", str2);

    scanf("%d", &i);

    ptr1 = str1 + i; // Перемещаем указатель ptr с начало str1 на i позиций вправо. Это не прокатило бы с указателем str1, т.к. его нельзя менять

    buf = *ptr1; // В buf теперь значение, взятое из переменной, на которую указывает ptr1
    *(ptr1) = *(ptr2 + i); // ptr2 все еще указывает на начало str2, поэтому прибавляем к нему i
    *(ptr2 + i) = buf;

    printf("result\n%s\n", str1);
    printf("%s\n", str2);

    return 0;
}

